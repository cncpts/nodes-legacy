echo "sets env vars"
source ./.env
echo "deploying ${PROJECT_NAME} stack..."
docker stack deploy -c ./docker-compose.yml ${PROJECT_NAME}
echo "finished"
