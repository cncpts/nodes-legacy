-- Verify aprt:auth_schema on pg

BEGIN;

DO $$
BEGIN
    --ASSERT auth.sign_jwt('{"role":"my_role","exp":"123456"}') = null;
    PERFORM settings.set ('jwt_secret', 'somejibrish');
    ASSERT auth.sign_jwt('{"role":"my_role","exp":"123456"}') = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoibXlfcm9sZSIsImV4cCI6IjEyMzQ1NiJ9.ujyiqZzXZzAZh-AJNvAWM5LRLTbjd-EIR70wiKIVzS4';
END $$;

ROLLBACK;
