-- Revert aprt:base_roles from pg
\set authenticator authenticator
\set anonymous anonymous

BEGIN;

drop owned by :authenticator;
drop role if exists :authenticator;

drop owned by :anonymous;
drop role if exists :anonymous;

DO
$do$
declare r record;
BEGIN
  for r in
     select unnest(enum_range(null::data.user_role)::text[]) as role
  loop
     execute 'drop owned by ' || quote_ident(r.role);
     execute 'drop role if exists ' || quote_ident(r.role);
  end loop;
END
$do$;

COMMIT;
