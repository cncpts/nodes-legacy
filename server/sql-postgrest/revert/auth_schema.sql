-- Revert aprt:auth_schema from pg

BEGIN;

-- drop auth schema and everything in it
DROP SCHEMA IF EXISTS auth CASCADE;

COMMIT;
