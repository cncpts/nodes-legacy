-- Revert nodes-postgrest:event_model from pg

BEGIN;

DROP TABLE IF EXISTS api.event;

COMMIT;
