-- Revert nodes-postgrest:concept_model from pg

BEGIN;

DROP TABLE IF EXISTS api.concept;

COMMIT;
