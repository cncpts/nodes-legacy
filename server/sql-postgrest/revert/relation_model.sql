-- Revert nodes-postgrest:relation_model from pg

BEGIN;

DROP TABLE IF EXISTS api.relation;

COMMIT;
