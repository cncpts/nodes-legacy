-- Deploy nodes-postgrest:reference_model to pg

BEGIN;

-- Table: api.reference

DROP TABLE IF EXISTS api.reference;

CREATE TABLE api.reference
(
    concept_id integer NOT NULL,
    reference_id serial NOT NULL,
    url text NOT NULL,
    PRIMARY KEY (reference_id),
    CONSTRAINT "no leading or trailing spaces" CHECK (length(url) = length(btrim(url))),
    FOREIGN KEY (concept_id)
        REFERENCES api.concept (concept_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

GRANT ALL ON TABLE api.reference TO anonymous;
GRANT ALL ON TABLE api.reference TO webuser;
GRANT ALL ON SEQUENCE api.reference_reference_id_seq TO anonymous;
GRANT ALL ON SEQUENCE api.reference_reference_id_seq TO webuser;

COMMIT;
