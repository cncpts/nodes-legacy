-- Deploy nodes-postgrest:concept_model to pg

BEGIN;

-- Table: api.concept

DROP TABLE IF EXISTS api.concept;

CREATE TABLE api.concept
(
    concept_id serial NOT NULL,
    concept_name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT concept_pkey PRIMARY KEY (concept_id),
    CONSTRAINT "no leading or trailing spaces" CHECK (length(concept_name) = length(btrim(concept_name)))
);

GRANT ALL ON TABLE api.concept TO anonymous;
GRANT ALL ON TABLE api.concept TO webuser;
GRANT ALL ON SEQUENCE api.concept_concept_id_seq TO anonymous;
GRANT ALL ON SEQUENCE api.concept_concept_id_seq TO webuser;

COMMIT;
