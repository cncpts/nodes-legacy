-- Deploy nodes-postgrest:relation_model to pg

BEGIN;

-- Table: api.relation

DROP TABLE IF EXISTS api.relation;

CREATE TABLE api.relation
(
    from_concept integer NOT NULL,
    to_concept integer NOT NULL,
    relation_description text NOT NULL,
    PRIMARY KEY (from_concept, to_concept),
    CONSTRAINT "no leading or trailing spaces" CHECK (length(relation_description) = length(btrim(relation_description))),
    FOREIGN KEY (from_concept)
        REFERENCES api.concept (concept_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    FOREIGN KEY (to_concept)
        REFERENCES api.concept (concept_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

GRANT ALL ON TABLE api.relation TO anonymous;
GRANT ALL ON TABLE api.relation TO webuser;

COMMIT;
