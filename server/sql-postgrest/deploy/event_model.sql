-- Deploy nodes-postgrest:event_model to pg

BEGIN;

-- Table: api.event

DROP TABLE IF EXISTS api.event;

CREATE TABLE api.event
(
    event_id uuid NOT NULL DEFAULT gen_random_uuid(),
    "timestamp" timestamp(6) with time zone NOT NULL DEFAULT now(),
    "user" int references data.user(id) default request.user_id(),
    CONSTRAINT event_pkey PRIMARY KEY (event_id)
);

COMMIT;
