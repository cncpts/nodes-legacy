import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

import Node from '/imports/api/nodes/nodes.js';
import Link from '/imports/api/links/links.js';

import '../../components/editable/editable.js';
import '../../components/text-input/text-input.js';
import '../../components/link-tr/link-tr.js';
import '../../components/node-graph/node-graph.js';

import './node.html';

Template.Node.onCreated(function() {
  // state from url
  this.getNodeId = () => FlowRouter.getParam('_id');
  // subscriptions
  this.autorun(() => {
    this.subscribe('nodes.get', this.getNodeId());
    this.subscribe('nodes.all');
    this.subscribe('edges.all');
    this.subscribe('links.all');
    this.subscribe('tags.all');
  });
});

Template.Node.helpers({
  node() {
    const nodeId = Template.instance().getNodeId();
    return Node.findOne(nodeId);
  },
  allNodes() {
    return Node.find();
  },
  allLinks() {
    return Link.find();
  },
  removeLink() {
    const nodeId = Template.instance().getNodeId();
    const node = Node.findOne(nodeId);
    return (id) => {
      node.removeLink(id);
    };
  },
});

Template.Node.events({
  'dblclick .title'() {
    this.editing();
  },
  'dblclick .description'() {
    this.editing();
  },
  'submit .js-toggle-tag'(event, templateInstance) {
    event.preventDefault();
    const node = Node.findOne(templateInstance.getNodeId());
    const form = event.target;
    node.toggleEdge(form.title.value);
    form.title.value = '';
  },
  'submit .js-add-link'(event, templateInstance) {
    event.preventDefault();
    const node = Node.findOne(templateInstance.getNodeId());
    const form = event.target;

    const newLink = {
      url: form.url.value.toLowerCase(),
    };

    const existLink = Link.findOne({ url: newLink.url }); // TODO: better duplication checks

    if (existLink) {
      node.addLink(existLink._id);
    } else {
      const newLinkId = new Link(newLink).create();
      node.addLink(newLinkId);
    }

    form.url.value = '';
  },
});
