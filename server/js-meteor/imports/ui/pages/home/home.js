import { Template } from 'meteor/templating';
import vis from 'vis';
import Node from '/imports/api/nodes/nodes.js';
import Edge from '/imports/api/edges/edges.js';

import './home.html';

Template.Home.onCreated(function() {
  this.autorun(() => {
    const nodesSubscription = this.subscribe('nodes.all');
    const edgesSubscription = this.subscribe('edges.all');
    if (nodesSubscription.ready() && edgesSubscription.ready()) {
      const nodes = Node.find();
      const edges = Edge.find();

      const visNodes = nodes.map(n => ({ id: n._id, label: n.title }));
      const visEdges = edges.map(e => ({ from: e.from, to: e.to, arrows: 'to' }));

      // create a network
      const container = document.getElementById('tree');
      const data = {
        nodes: new vis.DataSet(visNodes),
        edges: new vis.DataSet(visEdges),
      };
      const options = {};

      // initialize your network!
      const network = new vis.Network(container, data, options);
    }
  });
});
