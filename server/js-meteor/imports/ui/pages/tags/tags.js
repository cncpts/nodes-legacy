import { Template } from 'meteor/templating';

import Tag from '/imports/api/tags/tags.js';

import '../../components/selectable/selectable.js';
import '../../components/editable-td/editable-td.js';
import '../../components/text-input/text-input.js';
import './tags.html';

Template.Tags.onCreated(function() {
  // subscriptions
  this.autorun(() => {
    this.subscribe('tags.all');
  });
  // state
  this.state = new ReactiveDict();
  this.state.setDefault({
    selected: [],
  });
});

Template.Tags.helpers({
  allTags() {
    return Tag.find();
  },
  getObjectType(t) {
    return ['Edge', 'Link'][t];
  },
  hasSelected() {
    return Template.instance().state.get('selected').length > 0;
  },
  onSelect(item) {
    const state = Template.instance().state;
    // it seems that the template make two function-applys
    // so I wrapped the actual function with a lambda
    return () => (isSelected) => {
      const currentSelected = state.get('selected');
      const updatedSelected = isSelected
        ? [...currentSelected, item]
        : currentSelected.filter(itm => itm._id !== item._id);
      state.set('selected', updatedSelected);
    };
  },
});

Template.Tags.events({
  'submit .new-item'(event) {
    event.preventDefault();
    const form = event.target;
    const newItemId = new Tag({
      title: form.title.value,
      description: form.description.value,
      objectType: parseInt(form.type.value, 10),
    }).create();

    form.title.value = '';
    form.description.value = '';
    form.type.value = '0';
  },
  'submit .action'(event, templateInstance) {
    event.preventDefault();
    const action = event.target.action.value;
    const selected = templateInstance.state.get('selected');

    const updatedSelected = selected.filter((item) => {
      switch (action) {
        case 'REMOVE':
          item.delete();
          return false;
        default:
          return true;
      }
    });

    templateInstance.state.set('selected', updatedSelected);
  },
});
