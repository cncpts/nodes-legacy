import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import Tag from '/imports/api/tags/tags.js';

import './edge-tag-form.html';


Template.edgeTagForm.onCreated(function() {
  this.autorun(() => {
    Meteor.subscribe('tags.edge');
  });
});

Template.edgeTagForm.helpers({
  allTags() {
    return Tag.find({ objectType: 0 });
  },
});

Template.edgeTagForm.events({
  'submit form'(event) {
    event.preventDefault();
    const title = event.target.tag.value;
    this.item.toggleTag(title);
    this.doneEditing();
  },
  'blur input'() {
    this.doneEditing();
  },
});

Template.edgeTagForm.onRendered(function() {
  this.$('input').focus();
});
