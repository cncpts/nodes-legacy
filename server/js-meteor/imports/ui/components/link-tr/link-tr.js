import { Template } from 'meteor/templating';

import '../editable-td/editable-td.js';

import './link-tr.html';


Template.linkTr.events({
  'click .js-remove'() {
    const id = this.link._id;
    this.remove(id);
  },
});
