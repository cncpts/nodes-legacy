import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import Tag from '/imports/api/tags/tags.js';

import './link-tag-form.html';


Template.linkTagForm.onCreated(function() {
  this.autorun(() => {
    Meteor.subscribe('tags.link');
  });
});

Template.linkTagForm.helpers({
  allTags() {
    return Tag.find({ objectType: 1 });
  },
});

Template.linkTagForm.events({
  'submit form'(event) {
    event.preventDefault();
    const title = event.target.tag.value;
    this.item.toggleTag(title);
    this.doneEditing();
  },
  'blur input'() {
    this.doneEditing();
  },
});

Template.linkTagForm.onRendered(function() {
  this.$('input').focus();
});
