import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';
import Node from '../nodes/nodes.js';
import Tag from '../tags/tags.js';

const Edges = new Mongo.Collection('edges');

export default Class.create({
  name: 'Edge',
  collection: Edges,
  fields: {
    tags: {
      type: [String],
      default() {
        return [];
      },
    },
    from: {
      type: String,
    },
    to: {
      type: String,
    },
    createdAt: {
      type: Date,
      default() {
        return new Date();
      },
    },
  },
  helpers: {
    getTags() {
      return Tag.find({ _id: { $in: this.tags } });
    },
    getFromNode() {
      return Node.findOne(this.from);
    },
    getToNode() {
      return Node.findOne(this.to);
    },
  },
  meteorMethods: {
    create() {
      return this.save();
    },
    update(fields) {
      this.set(fields);
      return this.save();
    },
    delete() {
      return this.remove();
    },
    addTagId(tagId) {
      const tags$ = [...this.tags, tagId];
      this.tags = tags$;
      return this.save();
    },
    removeTagId(tagId) {
      const tags$ = this.tags.filter(t => t !== tagId);
      this.tags = tags$;
      return this.save();
    },
    toggleTag(tagTitle) {
      const tag = { title: tagTitle, objectType: 0 };
      const existTag = Tag.findOne(tag); // TODO: better duplication checks
      if (existTag) {
        const tagId = existTag._id;
        this.tags.includes(tagId) ? this.removeTagId(tagId) : this.addTagId(tagId);
      } else {
        console.log('No such tag please choose from existing tags');
      }
    },
  },
  events: {
    beforeSave(e) {
      const edge = e.currentTarget;
      // TODO: better duplication checks
      const similarItems = Edges.find({ from: edge.from, to: edge.to }).fetch();
      const similarItemsFilteredThis = similarItems.filter(i => i._id !== edge._id);
      if (similarItemsFilteredThis.length > 0 || edge.from === edge.to) {
        e.preventDefault();
        console.log('Tag already exists');
      }
    },
  },
});
