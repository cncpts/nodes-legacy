import { Mongo } from 'meteor/mongo';
import { Class, Enum } from 'meteor/jagi:astronomy';
import Edge from '../edges/edges.js';
import Link from '../links/links.js';

const Tags = new Mongo.Collection('tags');

const MapObjectTypeToCollection = {
  EDGE: Edge,
  LINK: Link,
};

const ObjectType = Enum.create({
  name: 'ObjectType',
  identifiers: ['EDGE', 'LINK'],
});

export default Class.create({
  name: 'Tag',
  collection: Tags,
  fields: {
    title: {
      type: String,
    },
    description: {
      type: String,
      default: '',
    },
    objectType: {
      type: ObjectType,
    },
    createdAt: {
      type: Date,
      default() {
        return new Date();
      },
    },
  },
  helpers: {
    getObjects() {
      const collection = MapObjectTypeToCollection[this.objectType];
      return collection.find({ tags: this._id });
    },
  },
  meteorMethods: {
    create() {
      return this.save();
    },
    update(fields) {
      this.set(fields);
      return this.save();
    },
    delete() {
      return this.remove();
    },
  },
  events: {
    beforeSave(e) {
      const tag = e.currentTarget;
      // TODO: better duplication checks
      const similarItems = Tags.find({ title: tag.title, objectType: tag.objectType }).fetch();
      const similarItemsFilteredThis = similarItems.filter(i => i._id !== tag._id);
      if (similarItemsFilteredThis.length > 0) {
        e.preventDefault();
        console.log('Tag already exists');
      }
    },
  },
});
