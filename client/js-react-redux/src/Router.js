import React from "react";
import { history } from "./Store";
import { ConnectedRouter } from "react-router-redux";
import { Switch, Route } from "react-router-dom";

import Layout from "./components/Layout";
import Home from "./components/Home";
import Search from "./components/Search";
import Login from "./components/Login";
import NodePage from "./components/Node";

export default () => (
  <ConnectedRouter history={history}>
    <Layout>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/search" component={Search} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Login} />
        <Route path="/node" component={NodePage} />
      </Switch>
    </Layout>
  </ConnectedRouter>
);
