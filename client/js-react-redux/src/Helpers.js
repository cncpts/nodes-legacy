import "rxjs";
import { ajax } from "rxjs/observable/dom/ajax";
import { createActions } from "redux-actions";

export const apiEpic = ({ triger, url, onSucc, onFail }) => action$ =>
  action$.ofType(triger).mergeMap(({ payload }) =>
    ajax
      .getJSON(url(payload))
      .map(onSucc)
      .catch(onFail)
  );

export const { noop } = createActions("NOOP");
