import React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import withQuery from "with-query";
import { Navbar as BsNavbar, Form, Input } from "reactstrap";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import logo from "./logo.png";
import { search } from "../processes/Search";

const mapDispatchToProps = dispatch => {
  return {
    goHome: () => dispatch(push("/")),
    submitSearchForm: e => {
      e.preventDefault();
      const query = e.target.query.value;
      e.target.query.value = "";
      dispatch(search(query));
      dispatch(push(withQuery("/search", { q: query })));
    },
    goToLogin: () => dispatch(push("/login"))
  };
};

export const Navbar = connect(null, mapDispatchToProps)(
  ({ goHome, submitSearchForm, goToLogin }) => {
    return (
      <BsNavbar light className="bg-light py-2 px-2">
        <img
          className="clickable"
          src={logo}
          width="25"
          height="25"
          alt="Nodes"
          onClick={goHome}
        />
        <Form inline onSubmit={submitSearchForm}>
          <Input
            type="search"
            name="query"
            placeholder="Search"
            autoComplete="off"
            bsSize="sm"
            className="mr-sm-2"
          />
        </Form>
        <FontAwesomeIcon
          icon="user"
          className="clickable mr-2"
          onClick={goToLogin}
        />
      </BsNavbar>
    );
  }
);

const Layout = ({ children }) => (
  <div className="h-100">
    <Navbar />
    {children}
  </div>
);

export default Layout;
