import React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import { Container, Row, Col, Table } from "reactstrap";
import withQuery from "with-query";
import Color from "color";
import { fetchNode, FETCH_NODE_RESULT } from "../processes/ManageNode";

const colors = [
  "rgb(26, 188, 15)",
  "rgb(46, 204, 113)",
  "rgb(52, 152, 219)",
  "rgb(155, 89, 182)",
  "rgb(52, 73, 94)",
  "rgb(22, 160, 133)",
  "rgb(39, 174, 96)",
  "rgb(41, 128, 185)",
  "rgb(142, 68, 173)",
  "rgb(44, 62, 80)",
  "rgb(241, 196, 15)",
  "rgb(230, 126, 34)",
  "rgb(231, 76, 60)",
  "rgb(236, 240, 241)",
  "rgb(149, 165, 166)",
  "rgb(243, 156, 18)",
  "rgb(211, 84, 0)",
  "rgb(192, 57, 43)",
  "rgb(189, 195, 199)",
  "rgb(127, 140, 141)"
];

const randomRadius = () => Math.floor(Math.random() * 100 + 100);

export const mapNodeToView = node => ({
  id: node.node_id,
  name: node.node_name,
  radius: randomRadius(),
  color: colors[Math.floor(Math.random() * colors.length)]
});

export const Node = connect(null, (dispatch, { node }) => ({
  viewNode: e => {
    dispatch(fetchNode(node.id));
    dispatch(push(withQuery("/node", { id: node.id })));
  }
}))(({ node, viewNode }) => (
  <div
    className="animated zoomIn clickable node"
    onClick={viewNode}
    style={{
      width: `${node.radius}px`,
      height: `${node.radius}px`,
      background: node.color,
      fontSize: `${(node.radius - node.name.length) / 7}px`,
      color: `${Color(node.color).isLight() ? "black" : "white"}`
    }}
  >
    {node.name}
  </div>
));

export const ReferencesTable = ({ refs }) => (
  <Table bordered size="sm">
    <tbody>
      {refs.map(url => (
        <tr className="table-light" key={url}>
          <td>
            <a href={url}>{url}</a>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

const NodePage = connect(({ app }) => app.manageNode)(
  ({ fetchStatus, node, error }) =>
    fetchStatus === FETCH_NODE_RESULT.SUCCESS ? (
      <Container>
        <Row className="py-5">
          <Col className="d-flex justify-content-center">
            <Node node={mapNodeToView(node[0])} />
          </Col>
        </Row>
        <Row>
          <Col>
            <ReferencesTable refs={node[0].urls} />
          </Col>
        </Row>
      </Container>
    ) : (
      "ERROR"
    )
);

export default NodePage;
