import React from "react";
import { Button, ButtonGroup } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input } from "reactstrap";
import { connect } from "react-redux";
import { replace } from "react-router-redux";

const mapStateToProps = ({ router }) => {
  return { path: router.location.pathname };
};

const mapDispatchToProps = dispatch => {
  return {
    showLoginForm: () => dispatch(replace("/login")),
    showRegisterForm: () => dispatch(replace("/register"))
  };
};

const LoginNavs = connect(mapStateToProps, mapDispatchToProps)(
  ({ path, showLoginForm, showRegisterForm }) => {
    return (
      <FormGroup>
        <ButtonGroup className="w-100">
          <Button
            outline={path !== "/login"}
            className="w-100 clickable"
            color="primary"
            onClick={showLoginForm}
          >
            Log In
          </Button>
          <Button
            outline={path !== "/register"}
            className="w-100 clickable"
            color="primary"
            onClick={showRegisterForm}
          >
            Register
          </Button>
        </ButtonGroup>
      </FormGroup>
    );
  }
);

const LoginForms = connect(mapStateToProps, null)(({ path }) => {
  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        console.log(e);
      }}
    >
      <FormGroup>
        <Input type="email" name="email" placeholder="Email" />
      </FormGroup>
      <FormGroup>
        <Input type="password" name="password" placeholder="Password" />
      </FormGroup>
      {path === "/register" ? (
        <FormGroup>
          <Input
            type="password"
            name="confirmPassword"
            placeholder="Confirm Password"
          />
        </FormGroup>
      ) : null}
      <Button outline size="lg" block color="primary">
        {path === "/register" ? "Register" : "Log In"}
      </Button>
    </Form>
  );
});

const Login = () => {
  return (
    <Container className="h-75">
      <Row className="align-items-center justify-content-center h-100">
        <Col md="4" style={{ height: "270px" }}>
          <Row>
            <Col>
              <LoginNavs />
            </Col>
          </Row>
          <Row>
            <Col>
              <LoginForms />
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
