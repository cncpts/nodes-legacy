import React from "react";
import { connect } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import { Node, mapNodeToView } from "./Node";
import { SEARCH_RESULTS } from "../processes/Search";

const Search = connect(({ app }) => app.searchResults, null)(
  ({ status, results }) => (
    <Container className="py-3">
      {status === SEARCH_RESULTS.SUCCESS
        ? results.map((node, i) => (
            <Row key={node.node_id}>
              <Col
                className={`d-flex justify-content-${
                  i % 2 === 0 ? "start" : "end"
                }`}
              >
                <Node node={mapNodeToView(node)} />
              </Col>
            </Row>
          ))
        : ""}
    </Container>
  )
);

export default Search;
