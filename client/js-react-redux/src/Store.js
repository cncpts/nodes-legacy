import { createStore, combineReducers, applyMiddleware } from "redux";
import { handleActions } from "redux-actions";
import { createEpicMiddleware, combineEpics } from "redux-observable";
import createHistory from "history/createBrowserHistory";
import { routerReducer, routerMiddleware } from "react-router-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import {
  searchInitialState,
  searchReducer,
  searchEpic
} from "./processes/Search";
import {
  nodeInitialState,
  nodeReducer,
  fetchNodeEpic
} from "./processes/ManageNode";
import { hydratEpic } from "./processes/Navigate";

export const history = createHistory();

// Initial State
const initialState = {
  ...searchInitialState,
  ...nodeInitialState
};

// Updaters
const reducer = handleActions(
  {
    ...searchReducer,
    ...nodeReducer
  },
  initialState
);

// create our store
export default createStore(
  combineReducers({
    app: reducer,
    router: routerReducer
  }),
  composeWithDevTools(
    applyMiddleware(
      createEpicMiddleware(combineEpics(hydratEpic, searchEpic, fetchNodeEpic)),
      routerMiddleware(history)
    )
  )
);
