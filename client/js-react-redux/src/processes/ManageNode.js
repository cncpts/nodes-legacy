import { createActions } from "redux-actions";
import withQuery from "with-query";
import { apiEpic } from "../Helpers";

// Constants
export const FETCH_NODE_RESULT = {
  FETCHING: "FETCHING",
  SUCCESS: "SUCCESS",
  FAIL: "FAIL",
  NOOP: "NOOP"
};

// Initial State
export const nodeInitialState = {
  node: {
    status: FETCH_NODE_RESULT.NOOP
  }
};

// Actions
export const { fetchNode, fetchNodeSucc, fetchNodeFail } = createActions(
  "FETCH_NODE",
  "FETCH_NODE_SUCC",
  "FETCH_NODE_FAIL"
);

// Updaters
export const nodeReducer = {
  [fetchNode](state, { payload: id }) {
    return {
      ...state,
      manageNode: { fetchStatus: FETCH_NODE_RESULT.FETCHING }
    };
  },

  [fetchNodeSucc](state, { payload: node }) {
    return {
      ...state,
      manageNode: { fetchStatus: FETCH_NODE_RESULT.SUCCESS, node }
    };
  },

  [fetchNodeFail](state, { payload: error }) {
    return {
      ...state,
      manageNode: { fetchStatus: FETCH_NODE_RESULT.FAIL, error }
    };
  }
};

// Async Updaters
export const fetchNodeEpic = apiEpic({
  triger: fetchNode,
  url: id =>
    withQuery("http://localhost:5000/nodes_with_references", {
      node_id: `eq.${id}`
    }),
  onSucc: fetchNodeSucc,
  onFail: fetchNodeFail
});
