import { createActions } from "redux-actions";
import withQuery from "with-query";
import { apiEpic } from "../Helpers";

// Constants
export const SEARCH_RESULTS = {
  FETCHING: "FETCHING",
  SUCCESS: "SUCCESS",
  FAIL: "FAIL",
  NOOP: "NOOP"
};

// Initial State
export const searchInitialState = {
  searchResults: {
    status: SEARCH_RESULTS.NOOP
  }
};

// Actions
export const { search, searchSucc, searchFail } = createActions(
  "SEARCH",
  "SEARCH_SUCC",
  "SEARCH_FAIL"
);

// Updaters
export const searchReducer = {
  [search](state, { payload: query }) {
    return {
      ...state,
      searchResults: { status: SEARCH_RESULTS.FETCHING }
    };
  },

  [searchSucc](state, { payload: results }) {
    return {
      ...state,
      searchResults: { status: SEARCH_RESULTS.SUCCESS, results }
    };
  },

  [searchFail](state, { payload: error }) {
    return {
      ...state,
      searchResults: { status: SEARCH_RESULTS.FAIL, error }
    };
  }
};

// Async Updaters
export const searchEpic = apiEpic({
  triger: search,
  url: q =>
    withQuery("http://localhost:5000/nodes", { node_name: `ilike.*${q}*` }),
  onSucc: searchSucc,
  onFail: searchFail
});
