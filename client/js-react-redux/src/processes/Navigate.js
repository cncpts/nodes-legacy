import "rxjs";
import { ofType } from "redux-observable";
import { createActions } from "redux-actions";
import { push, replace } from "react-router-redux";
import * as R from "ramda";
import qs from "qs";
import { noop } from "../Helpers";
import { search as searchAction } from "./Search";
import { fetchNode } from "./ManageNode";

// Constants
export const PAGES = {
  HOME: "HOME",
  SEARCH: "SEARCH",
  LOGIN: "LOGIN",
  REGISTER: "REGISTER",
  NODE: "NODE"
};

const mapActionToHistoryAPI = {
  GO_TO_PAGE: push,
  NAV_IN_PAGE: replace
};

// Actions
export const { goToPage, navInPage } = createActions(
  "GO_TO_PAGE",
  "NAV_IN_PAGE"
);

// Async Updaters
export const navEpic = action$ =>
  action$.filter(!ofType(noop)).map(({ type, payload }) => {
    mapActionToHistoryAPI(type)(payload);
    return noop();
  });

export const hydratEpic = action$ =>
  action$
    .ofType("@@router/LOCATION_CHANGE") // take only route changes
    .filter(({ payload }) => R.isNil(payload.key)) // take only route changes that caused by the browser
    .map(action => mapPathToActions(action.payload));

const mapPathToActions = ({ hash, pathname, search, state }) => {
  return R.cond([
    [R.equals("/search"), () => searchAction(qs.parse(R.tail(search)).q)],
    [R.equals("/node"), () => fetchNode(qs.parse(R.tail(search)).id)],
    [R.T, noop]
  ])(pathname);
};
