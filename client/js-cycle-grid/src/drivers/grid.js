import Rx from 'rxjs';
import * as R from 'ramda';

class EventBus {
  constructor() {
    this.subject = new Rx.ReplaySubject();
  }

  subscribe(key) {
    if (R.isNil(key)) {
      throw new Error('Check your subscribe calls because i got undefind in the subscription key');
    }
    return this.subject.filter(event => event.key === key).map(event => event.val);
  }

  register(key, event$) {
    if (R.isNil(key)) {
      throw new Error('Check your register calls because i got undefind in the registeration key');
    }
    if (R.isNil(event$)) {
      throw new Error('Check your register calls because i got undefind in the registeration stream');
    }
    event$
      .map(e => ({ key, val: e }))
      .do(console.log)
      .subscribe((e) => {
        this.subject.next(e);
      });
  }
}

export default function makeGridDriver() {
  const eventBus = new EventBus();

  function gridDriver() {
    return eventBus;
  }

  return gridDriver;
}
