import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';

function main({ grid, DOM }) {
  const navigate$ = DOM.select('[data-action=navigate]')
    .events('click')
    .map(R.pipe(R.path(['target', 'dataset']), ds => Object.assign({}, ds)));

  const formSubmission$ = DOM.select('form')
    .events('submit', { preventDefault: true })
    .map(({ target }) => ({
      action: target.dataset.action,
      data: U.formsExtraction[target.dataset.action](target),
    }));

  const actions$ = Rx.Observable.merge(navigate$, formSubmission$);

  grid.register('actions', actions$);
}

export default main;
