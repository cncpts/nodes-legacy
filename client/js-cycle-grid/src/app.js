import * as R from 'ramda';
import * as U from './utilities';
import Actions from './actions';

import Router from './cells/router';
import Search from './cells/search';

import Layout from './views/layout';
import Navbar from './views/navbar';
import Home from './views/home';
import LoginArea from './views/login-area';
import NodeView from './views/node';

export default function App(sources) {
  [Actions, Router, Search, LoginArea, Layout, Navbar, Home, NodeView].map(R.applyTo(sources));

  const sinks = {
    DOM: sources.grid.subscribe(U.drivers.DOM),
    HTTP: sources.grid.subscribe(U.drivers.HTTP),
    history: sources.grid.subscribe(U.drivers.history),
  };
  return sinks;
}
