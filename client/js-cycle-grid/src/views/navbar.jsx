import Rx from 'rxjs';
import { h } from '@cycle/dom';
import * as U from '../utilities';

const navbar = () => (
  <nav className="navbar navbar-light bg-light py-2 px-2">
    <img
      className="clickable"
      src="/icon.png"
      width="25"
      height="25"
      alt=""
      data-action={U.actions.navigate}
      data-route={U.routes.home}
    />
    <form className="form-inline" data-action={U.actions.search}>
      {h('input.form-control.form-control-sm.mr-sm-2', {
        attrs: {
          type: 'search',
          name: 'query',
          placeholder: 'Search',
          autoComplete: 'off',
        },
      })}
    </form>
    <i
      className="fa fa-user clickable mr-2"
      aria-hidden="true"
      data-action={U.actions.navigate}
      data-route={U.routes.login}
    />
  </nav>
);

function main({ grid }) {
  const navbar$ = Rx.Observable.of(navbar());
  // register the view as DOM
  grid.register(U.views.navbar, navbar$);
}

export default main;
