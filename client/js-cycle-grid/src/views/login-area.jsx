import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';

const navs = route => (
  <div className="btn-group w-100" data-toggle="buttons">
    <label
      className={`btn btn-primary clickable w-100 ${
        R.equals(route, U.routes.login) ? 'active' : ''
      }`}
      htmlFor="login"
    >
      <input
        name="options"
        id="login"
        type="radio"
        data-action={U.actions.navigate}
        data-route={U.routes.login}
      />
      Login
    </label>
    <label
      className={`btn btn-primary clickable w-100 ${
        R.equals(route, U.routes.register) ? 'active' : ''
      }`}
      htmlFor="register"
    >
      <input
        name="options"
        id="register"
        type="radio"
        data-action={U.actions.navigate}
        data-route={U.routes.register}
      />
      Register
    </label>
  </div>
);

function main({ grid }) {
  const route$ = grid.subscribe(U.streams.route);
  const loginArea$ = Rx.Observable.combineLatest(route$).map(([route]) => (
    <main className="container h-100">
      <div className="row align-items-center justify-content-center h-100">
        <div className="col-md-4" style={{ height: '270px' }}>
          <div className="row">
            <div className="col">{navs(route)}</div>
          </div>
          <div className="row">
            <div className="col">
              <form
                data-action={R.cond([
                  [R.equals(U.routes.register), R.always('register')],
                  [R.equals(U.routes.login), R.always('login')],
                ])(route)}
              >
                <div className="form-group">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Enter email"
                    name="email"
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    name="password"
                    required
                  />
                </div>
                {R.equals(route, U.routes.register) ? (
                  <div className="form-group">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Confirm password"
                      name="confirm_password"
                      required
                    />
                  </div>
                ) : null}
                <button className="btn btn-outline-primary btn-lg btn-block clickable">
                  {R.cond([
                    [R.equals(U.routes.register), R.always('Register')],
                    [R.equals(U.routes.login), R.always('Login')],
                  ])(route)}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </main>
  ));
  grid.register(U.views.login_area, loginArea$);
}

export default main;
