import Rx from 'rxjs';
import Color from 'color';
import qs from 'qs';
import * as R from 'ramda';
import * as U from '../utilities';

const colors = [
  'rgb(26, 188, 15)',
  'rgb(46, 204, 113)',
  'rgb(52, 152, 219)',
  'rgb(155, 89, 182)',
  'rgb(52, 73, 94)',
  'rgb(22, 160, 133)',
  'rgb(39, 174, 96)',
  'rgb(41, 128, 185)',
  'rgb(142, 68, 173)',
  'rgb(44, 62, 80)',
  'rgb(241, 196, 15)',
  'rgb(230, 126, 34)',
  'rgb(231, 76, 60)',
  'rgb(236, 240, 241)',
  'rgb(149, 165, 166)',
  'rgb(243, 156, 18)',
  'rgb(211, 84, 0)',
  'rgb(192, 57, 43)',
  'rgb(189, 195, 199)',
  'rgb(127, 140, 141)',
];

const randomRadius = () => Math.floor(Math.random() * 100 + 100);

export const mapNodeToView = node => ({
  id: node.node_id,
  name: node.node_name,
  radius: randomRadius(),
  color: colors[Math.floor(Math.random() * colors.length)],
});

export const Node = ({
  id, name, radius, color,
}) => (
  <div
    className="animated zoomIn clickable circle-width-text"
    data-action={U.actions.navigate}
    data-pathname={`${U.routes.node}?${qs.stringify({ id })}`}
    style={{
      width: `${radius}px`,
      height: `${radius}px`,
      background: color,
      fontSize: `${(radius - name.length) / 7}px`,
      color: `${Color(color).isLight() ? 'black' : 'white'}`,
    }}
  >
    {name}
  </div>
);

export const ReferencesTable = refs => (
  <table className="table">
    <tbody>
      {refs.map(url => (
        <td>
          <a href={url}>{url}</a>
        </td>
      ))}
    </tbody>
  </table>
);

export const NodePage = node => (
  <div>
    <div className="row">
      <div className="col d-flex justify-content-center">{Node(mapNodeToView(node))}</div>
    </div>
    <div className="row">{ReferencesTable(node.urls)}</div>
  </div>
);

function main({ grid }) {
  const node$ = Rx.Observable.of('');
  grid.register(U.views.node, node$);
}

export default main;
