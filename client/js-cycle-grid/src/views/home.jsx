import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';
import { Node, mapNodeToView } from './node';

const home = nodes => (
  <main role="main" className="container py-3">
    {nodes.map((node, i) => (
      <div className="row">
        <div className={`col d-flex justify-content-${i % 2 === 0 ? 'start' : 'end'}`}>
          {Node(mapNodeToView(node))}
        </div>
      </div>
    ))}
  </main>
);

function main({ grid }) {
  const result$ = grid.subscribe('SearchResponse');

  const home$ = Rx.Observable.of([])
    .merge(result$)
    .map(home);

  grid.register(U.views.home, home$);
}

export default main;
