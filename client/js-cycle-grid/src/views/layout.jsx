import Rx from 'rxjs';
import * as U from '../utilities';

function main({ grid }) {
  const navbar$ = grid.subscribe(U.views.navbar);
  const routerView$ = grid.subscribe(U.streams.router_view);

  const layout$ = Rx.Observable.combineLatest(navbar$, routerView$).map(([navbar, routerView]) => (
    <div className="d-flex flex-column w-100 h-100">
      {navbar}
      {routerView}
    </div>
  ));

  grid.register(U.views.layout, layout$);
}

export default main;
