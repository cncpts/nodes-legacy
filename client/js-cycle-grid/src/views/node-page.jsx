import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';
import { Node, mapNodeToView } from './node';

function main({ grid }) {
  grid.register(U.views.home, home$);
}

export default main;
