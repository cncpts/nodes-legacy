import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';

function main({ grid, HTTP }) {
  // Parse routing events
  const query$ = grid
    .subscribe('actions')
    .filter(R.propEq('action', U.actions.search))
    .map(R.path(['data', 'query']));

  const httpRequest$ = query$.map(q => ({
    category: U.http.search,
    url: 'http://localhost:3000/nodes',
    query: {
      node_name: `ilike.*${q}*`,
    },
  }));

  const httpResponse$ = HTTP.select(U.http.search)
    .switch()
    .map(R.prop('body'));

  grid.register('HTTP', httpRequest$);
  grid.register('SearchResponse', httpResponse$);
}

export default main;
