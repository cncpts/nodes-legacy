import Rx from 'rxjs';
import * as R from 'ramda';
import * as U from '../utilities';

function main({ grid, history }) {
  // Parse routing events
  const historyDriverPath$ = history.do(h => console.log('HISTORY: ', h)).map(R.prop('pathname'));
  const navigateActions$ = grid
    .subscribe('actions')
    .filter(R.propEq('action', U.actions.navigate))
    .map(R.prop('pathname'));

  const route$ = Rx.Observable.merge(historyDriverPath$, navigateActions$).distinctUntilChanged();

  // Map current route to view
  const home$ = grid.subscribe(U.views.home);
  const login$ = grid.subscribe(U.views.login_area);
  const node$ = grid.subscribe(U.views.node);

  const routerView$ = Rx.Observable.combineLatest(route$, home$, login$, node$).map(([route, home, login, node]) =>
    R.prop(
      route,
      R.fromPairs([
        [U.routes.home, home],
        [U.routes.login, login],
        [U.routes.register, login],
        [U.routes.node, node],
      ]),
    ));

  grid.register(U.drivers.history, route$.skip(1).sample(navigateActions$));
  grid.register(U.streams.route, route$);
  grid.register(U.streams.router_view, routerView$);
}

export default main;
