import { run } from '@cycle/rxjs-run';
import { makeDOMDriver } from '@cycle/dom';
import { makeHistoryDriver } from '@cycle/history';
import { makeHTTPDriver } from '@cycle/http';
import makeGridDriver from './drivers/grid';
import App from './app';

const main = App;

const drivers = {
  DOM: makeDOMDriver('#root'),
  grid: makeGridDriver(),
  history: makeHistoryDriver(),
  HTTP: makeHTTPDriver(),
};

run(main, drivers);
