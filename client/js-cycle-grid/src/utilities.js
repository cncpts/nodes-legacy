import * as R from 'ramda';

export const routes = R.fromPairs([
  ['home', '/'],
  ['login', '/login'],
  ['register', '/register'],
  ['node', '/node'],
]);

export const actions = R.fromPairs([
  ['navigate', 'navigate'],
  ['login', 'login'],
  ['register', 'register'],
  ['search', 'search'],
]);

export const streams = R.fromPairs([['route', 'route'], ['router_view', 'router_view']]);

export const views = R.fromPairs([
  ['layout', 'layout'],
  ['navbar', 'navbar'],
  ['home', 'home'],
  ['login_area', 'login_area'],
  ['node', 'node'],
]);

export const http = R.fromPairs([['search', 'search']]);

export const drivers = R.fromPairs([['DOM', 'layout'], ['history', 'history'], ['HTTP', 'HTTP']]);

export const formToRecord = R.curry((fields, form) => {
  const values = fields.map(f => form[f].value);
  return R.zipObj(fields, values);
});

export const formsExtraction = R.fromPairs([
  [actions.login, formToRecord(['email', 'password'])],
  [actions.register, formToRecord(['email', 'password', 'confirm_password'])],
  [actions.search, formToRecord(['query'])],
]);
