module Components.Layout exposing (..)

import Msgs exposing (..)
import Models exposing (..)
import Utils exposing (sendMsg)
import Html exposing (..)
import Html.Attributes exposing (class)


init : ( Layout, Cmd Msg )
init =
    ( div [] [], Cmd.none )


html : IncBtn -> DecBtn -> Counter -> Layout
html incBtn decBtn counter =
    div [ class "container full-height" ]
        [ div
            [ class "row full-height justify-content-center align-items-center" ]
            [ div
                [ class "col-lg-4" ]
                [ div
                    [ class "row" ]
                    [ h1 [ class "count mx-auto" ]
                        [ counter |> toString |> Html.text ]
                    ]
                , div
                    [ class "row" ]
                    [ div
                        [ class "col" ]
                        [ incBtn ]
                    , div
                        [ class "col" ]
                        [ decBtn ]
                    ]
                ]
            ]
        ]


update : Msg -> Grid -> ( Layout, Cmd Msg )
update msg grid =
    case msg of
        IncBtn ->
            ( html grid.incBtn grid.decBtn grid.counter, sendMsg Layout )

        DecBtn ->
            ( html grid.incBtn grid.decBtn grid.counter, sendMsg Layout )

        Counter ->
            ( html grid.incBtn grid.decBtn grid.counter, sendMsg Layout )

        _ ->
            ( grid.layout, Cmd.none )
