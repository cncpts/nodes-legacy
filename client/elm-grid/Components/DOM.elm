module Components.DOM exposing (..)

import Msgs exposing (..)
import Models exposing (..)
import Utils exposing (sendMsg, stylesheet, script)
import Html exposing (..)
import Html.Attributes exposing (class)


init : ( DOM, Cmd Msg )
init =
    ( div [ class "full-height" ] []
    , Cmd.none
    )


update : Msg -> Grid -> ( Layout, Cmd Msg )
update msg grid =
    case msg of
        Layout ->
            ( html grid.layout, sendMsg DOM )

        _ ->
            ( grid.dom, Cmd.none )


html : Layout -> DOM
html layout =
    div [ class "full-height" ]
        [ layout ]
