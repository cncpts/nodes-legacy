module Components.LoginForm exposing (..)

import Msgs exposing (..)
import Models exposing (..)
import Utils exposing (sendMsg)


init : ( LoginForm, Cmd Msg )
init =
    ( Models.LoginForm "" "", Cmd.none )


update : Msg -> Grid -> ( LoginForm, Cmd Msg )
update msg grid =
    case msg of
        ChangeUsername username ->
            let
                loginForm =
                    grid.loginForm
            in
                ( { loginForm | username = username }, sendMsg Msgs.LoginForm )

        ChangePassword password ->
            let
                loginForm =
                    grid.loginForm
            in
                ( { loginForm | password = password }, sendMsg Msgs.LoginForm )

        _ ->
            ( grid.loginForm, Cmd.none )
