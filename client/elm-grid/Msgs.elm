module Msgs exposing (..)

-- MSGS


type Msg
    = IncBtn
    | DecBtn
    | Counter
    | LoginForm
    | Layout
    | DOM
      -- DOM Events
    | ChangeUsername String
    | ChangePassword String
    | IncBtnClick
    | DecBtnClick
