import * as React from "react";
import * as ReactDOM from "react-dom";
import { observable, computed, action, createTransformer } from "mobx";
import { observer } from "mobx-react";
import DevTools from "mobx-react-devtools";
import * as C from "./constants";
import "../static/css/litera.css";
import * as Rs from "reactstrap";
import * as R from "ramda";
import createHistory from "history/createBrowserHistory";

class App {
  constructor() {
    this.DriverHistory = createHistory();
    this.CoreDataLocation = this.DriverHistory.location;
    this.DriverHistory.listen((location, action) => {
      this.CoreDataLocation = location;
    });
    this.DriverHistory.replace(C.routes.login);
  }

  // Drivers
  DriverHistory: any;

  // Core Data
  @observable CoreDataLocation: any;
  @observable
  CoreDataSignForm = {
    email: "",
    password: "",
    confirmPassword: ""
  };

  // Derived Data

  // Views
  @computed
  get ViewSignForm() {
    const path = this.CoreDataLocation.pathname;
    return (
      <Rs.Form onSubmit={this.submitLoginForm}>
        <Rs.FormGroup>
          <Rs.Input
            type="email"
            name="email"
            placeholder="Email"
            value={this.CoreDataSignForm.email}
            onChange={this.changeLoginFormEmail}
          />
        </Rs.FormGroup>
        <Rs.FormGroup>
          <Rs.Input
            type="password"
            name="password"
            placeholder="Password"
            value={this.CoreDataSignForm.password}
            onChange={this.changeLoginFormPassword}
          />
        </Rs.FormGroup>
        {R.equals(path, C.routes.register) ? (
          <Rs.FormGroup>
            <Rs.Input
              type="password"
              name="confirmPassword"
              placeholder="Confirm password"
              value={this.CoreDataSignForm.confirmPassword}
              onChange={this.changeLoginFormConfirmPassword}
            />
          </Rs.FormGroup>
        ) : null}
        <Rs.Button outline color="primary" size="lg" block>
          {R.cond([
            [R.equals(C.routes.register), R.always("Register")],
            [R.equals(C.routes.login), R.always("Login")]
          ])(path)}
        </Rs.Button>
      </Rs.Form>
    );
  }

  @computed
  get ViewSignPage() {
    const path = this.CoreDataLocation.pathname;
    return (
      <Rs.Container className="full-height">
        <Rs.Row className="align-items-center justify-content-center full-height">
          <Rs.Col md="4" style={{ height: "270px" }}>
            <Rs.Row>
              <Rs.Col>
                <div className="btn-group full-width" data-toggle="buttons">
                  <label
                    className={`btn btn-primary full-width ${
                      R.equals(path, C.routes.login) ? "active" : ""
                    }`}
                  >
                    <input
                      name="options"
                      id="login"
                      type="radio"
                      onClick={this.navigateTo(C.routes.login)}
                    />
                    Login
                  </label>
                  <label
                    className={`btn btn-primary full-width ${
                      R.equals(path, C.routes.register) ? "active" : ""
                    }`}
                  >
                    <input
                      name="options"
                      id="register"
                      type="radio"
                      onClick={this.navigateTo(C.routes.register)}
                    />
                    Register
                  </label>
                </div>
              </Rs.Col>
            </Rs.Row>
            <Rs.Row>
              <Rs.Col>{this.ViewSignForm}</Rs.Col>
            </Rs.Row>
          </Rs.Col>
        </Rs.Row>
      </Rs.Container>
    );
  }

  @computed
  get ViewLayout() {
    return this.ViewSignPage;
  }

  @computed
  get DOMRoot() {
    return this.ViewLayout;
  }

  // Actions

  @action
  navigateTo = path => () => {
    this.DriverHistory.push(path);
    return this;
  };

  @action
  changeLoginFormEmail = e => {
    this.CoreDataSignForm.email = e.target.value;
    return this;
  };

  @action
  changeLoginFormPassword = e => {
    this.CoreDataSignForm.password = e.target.value;
    return this;
  };

  @action
  changeLoginFormConfirmPassword = e => {
    this.CoreDataSignForm.confirmPassword = e.target.value;
    return this;
  };

  @action
  submitLoginForm = e => {
    e.preventDefault();
    console.log(
      `email: ${this.CoreDataSignForm.email},
      password: ${this.CoreDataSignForm.password},
      confirm password: ${this.CoreDataSignForm.confirmPassword}`
    );
    return this;
  };
}

@observer
class DOM extends React.Component<{ app: App }, {}> {
  render() {
    return this.props.app.DOMRoot;
  }
}

const app = new App();
ReactDOM.render(<DOM app={app} />, document.getElementById("root"));
