import { Meteor } from 'meteor/meteor';
import Edge from '../edges.js';

Meteor.publish('edges.all', function () {
  return Edge.find();
});
