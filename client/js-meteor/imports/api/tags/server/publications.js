import { Meteor } from 'meteor/meteor';
import Tag from '../tags.js';

Meteor.publish('tags.all', function () {
  return Tag.find();
});

Meteor.publish('tags.edge', function () {
  return Tag.find({ objectType: 0 });
});

Meteor.publish('tags.link', function () {
  return Tag.find({ objectType: 1 });
});
