import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';
import Tag from '../tags/tags.js';

const Links = new Mongo.Collection('links');

export default Class.create({
  name: 'Link',
  collection: Links,
  fields: {
    title: {
      type: String,
      default: 'No title',
    },
    description: {
      type: String,
      default: 'No description',
    },
    url: {
      type: String,
    },
    tags: {
      type: [String],
      default() {
        return [];
      },
    },
    createdAt: {
      type: Date,
      default() {
        return new Date();
      },
    },
  },
  helpers: {
    getTags() {
      return Tag.find({ _id: { $in: this.tags } });
    },
  },
  meteorMethods: {
    create() {
      return this.save();
    },
    update(fields) {
      this.set(fields);
      return this.save();
    },
    delete() {
      return this.remove();
    },
    addTagId(tagId) {
      const tags$ = [...this.tags, tagId];
      this.tags = tags$;
      return this.save();
    },
    removeTagId(tagId) {
      const tags$ = this.tags.filter(t => t !== tagId);
      this.tags = tags$;
      return this.save();
    },
    toggleTag(tagTitle) {
      const tag = { title: tagTitle, objectType: 1 };
      const existTag = Tag.findOne(tag); // TODO: better duplication checks
      if (existTag) {
        const tagId = existTag._id;
        this.tags.includes(tagId) ? this.removeTagId(tagId) : this.addTagId(tagId);
      } else {
        console.log('No such tag please choose from existing tags');
      }
    },
  },
});
