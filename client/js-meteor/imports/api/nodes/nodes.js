// Definition of the nodes collection

import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';
import Edge from '../edges/edges.js';
import Link from '../links/links';

export const Nodes = new Mongo.Collection('nodes');

const Node = Class.create({
  name: 'Node',
  collection: Nodes,
  fields: {
    title: {
      type: String,
    },
    description: {
      type: String,
      default: 'No description',
    },
    links: {
      type: [String],
      default() {
        return [];
      },
    },
    createdAt: {
      type: Date,
      default() {
        return new Date();
      },
    },

    lastUpdated: {
      type: Date,
      default() {
        return new Date();
      },
    },
  },
  helpers: {
    getLinks() {
      return Link.find({
        _id: {
          $in: this.links,
        },
      });
    },
    getTags() {
      return this.getEdges().map(e => e.getToNode());
    },
    getEdges() {
      return Edge.find({ from: this._id });
    },
  },
  meteorMethods: {
    create() {
      return this.save();
    },
    update(fields) {
      this.set(fields);
      this.lastUpdated = new Date();
      return this.save();
    },
    delete() {
      return this.remove();
    },
    toggleEdge(nodeTitle) {
      const node = { title: nodeTitle };
      const existNode = Node.findOne(node); // TODO: better duplication checks
      const nodeId = existNode ? existNode._id : new Node(node).create();
      const edge = { from: this._id, to: nodeId };
      const existEdge = Edge.findOne(edge);
      const edgeId = existEdge ? existEdge.delete() : new Edge(edge).create();
      this.lastUpdated = new Date();
      return edgeId;
    },
    addLink(id) {
      this.links = [...this.links, id];
      this.lastUpdated = new Date();
      return this.save();
    },
    removeLink(id) {
      this.links = this.links.filter(i => i !== id);
      this.lastUpdated = new Date();
      return this.save();
    },
  },
});

export default Node;
