import { Template } from 'meteor/templating';

import Edge from '/imports/api/edges/edges.js';
import Node from '/imports/api/nodes/nodes.js';

import '../../components/selectable/selectable.js';
import '../../components/editable-td/editable-td.js';
import '../../components/text-input/text-input.js';
import '../../components/edge-node-form/edge-node-form.js';
import '../../components/edge-tag-form/edge-tag-form.js';
import './edges.html';

Template.edges.onCreated(function() {
  // subscriptions
  this.autorun(() => {
    this.subscribe('edges.all');
    this.subscribe('nodes.all');
    this.subscribe('tags.edge');
  });
  // state
  this.state = new ReactiveDict();
  this.state.setDefault({
    selected: [],
  });
});

Template.edges.helpers({
  allEdges() {
    return Edge.find();
  },
  allNodes() {
    return Node.find();
  },
  hasSelected() {
    return Template.instance().state.get('selected').length > 0;
  },
  onSelect(item) {
    const state = Template.instance().state;
    // it seems that the template make two function-applys
    // so I wrapped the actual function with a lambda
    return () => (isSelected) => {
      const currentSelected = state.get('selected');
      const updatedSelected = isSelected
        ? [...currentSelected, item]
        : currentSelected.filter(itm => itm._id !== item._id);
      state.set('selected', updatedSelected);
    };
  },
});

Template.edges.events({
  'submit .new-item'(event) {
    event.preventDefault();
    const form = event.target;
    const newItemId = new Edge({
      from: form.from.value,
      to: form.to.value,
    }).create();

    form.from.value = '';
    form.to.value = '';
  },
  'submit .action'(event, templateInstance) {
    event.preventDefault();
    const action = event.target.action.value;
    const selected = templateInstance.state.get('selected');

    const updatedSelected = selected.filter((item) => {
      switch (action) {
        case 'REMOVE':
          item.delete();
          return false;
        default:
          return true;
      }
    });

    templateInstance.state.set('selected', updatedSelected);
  },
});
