import { Template } from 'meteor/templating';

import Link from '/imports/api/links/links.js';

import '../../components/selectable/selectable.js';
import '../../components/editable-td/editable-td.js';
import '../../components/text-input/text-input.js';
import '../../components/link-tag-form/link-tag-form.js';

import './links.html';

Template.Links.onCreated(function() {
  // subscriptions
  this.autorun(() => {
    this.subscribe('links.all');
    this.subscribe('tags.link');
  })
  // state
  this.state = new ReactiveDict();
  this.state.setDefault({
    selected: [],
  });
});

Template.Links.helpers({
  allLinks() {
    return Link.find();
  },
  hasSelected() {
    return Template.instance().state.get('selected').length > 0;
  },
  onSelect(item) {
    const state = Template.instance().state;
    // it seems that the template make two function-applys
    // so I wrapped the actual function with a lambda
    return (() => (isSelected) => {
      const currentSelected = state.get('selected');
      const updatedSelected = isSelected
        ? [...currentSelected, item]
        : currentSelected.filter(itm => itm._id !== item._id);
      state.set('selected', updatedSelected);
    });
  },
});

Template.Links.events({
  'submit .js-new-item' (event) {
      event.preventDefault();
      const form = event.target;

      const newItem = {
        title: form.title.value,
        description: form.description.value,
        url: form.url.value.toLowerCase(),
      };

      const existItem = Link.findOne({url: newItem.url}) // TODO: better duplication checks

      if(existItem) {
        // TODO: alert something?
      } else {
        const newItemId = new Link(newItem).create();
      }

      form.title.value = "";
      form.description.value = "";
      form.url.value = "";
  },
  'submit .js-action' (event, instance) {
      event.preventDefault();
      const action = event.target.action.value;
      const selected = instance.state.get('selected');

      const updatedSelected = selected.filter(itm => {
        switch(action) {
          case "REMOVE":
              itm.delete();
              return false;
          default:
              return true;
        }
      });

      instance.state.set('selected', updatedSelected);
  },
});
