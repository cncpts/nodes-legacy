import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import Node from '/imports/api/nodes/nodes.js';

import './edge-node-form.html';


Template.edgeNodeForm.onCreated(function() {
  this.autorun(() => {
    Meteor.subscribe('nodes.all');
  });
});

Template.edgeNodeForm.helpers({
  allNodes() {
    return Node.find();
  },
});

Template.edgeNodeForm.events({
  'submit form'(event) {
    event.preventDefault();
    console.log(this.name);
    const value = event.target[this.name].value;
    console.log(value);
    this.doneEditing();
  },
  'blur input'() {
    this.doneEditing();
  },
});

Template.edgeNodeForm.onRendered(function() {
  this.$('input').focus();
});
