import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import vis from 'vis';

import Node from '/imports/api/nodes/nodes.js';

import './node-graph.html';

Template.nodeGraph.onCreated(function() {
  this.getNodeId = () => FlowRouter.getParam('_id');
  this.autorun(() => {
    const nodesSubscription = this.subscribe('nodes.all');
    const edgesSubscription = this.subscribe('edges.all');
    if (nodesSubscription.ready() && edgesSubscription.ready()) {
      const node = Node.findOne(this.getNodeId());
      const neighbors = node.getTags().map(n => ({
        id: n._id,
        label: n.title.split(' ').join('\n'),
        group: 1,
      }));
      const thisNode = {
        id: node._id,
        label: node.title.split(' ').join('\n'),
        color: '#7BE141',
        shape: 'circle',
        font: {
          size: 30,
        },
      };
      const edges = node.getEdges();

      const visNodes = [...neighbors, thisNode];
      const visEdges = edges.map(e => ({ from: e.from, to: e.to, arrows: 'to', label: e.getTags().map(t => t.title).join(' ') }));

      // create a network
      const container = document.getElementById('node-graph');
      const data = {
        nodes: new vis.DataSet(visNodes),
        edges: new vis.DataSet(visEdges),
      };
      const options = {
        height: '300px',
        width: '100%',
        nodes: {
          margin: 10,
          shape: 'box',
          font: {
            size: 16,
          },
          borderWidth: 2,
          shadow: true,
        },
        edges: {
          physics: false,
          font: { align: 'middle' },
        },
        layout: {
          hierarchical: {
            direction: 'UD',
            sortMethod: 'directed',
          },
        },
        interaction: {
          dragNodes: false,
          dragView: false,
          zoomView: false,
        },
      };

      // initialize your network!
      const network = new vis.Network(container, data, options);

      network.on('click', function(params) {
        if (params.nodes[0]) {
          FlowRouter.setParams({ _id: params.nodes[0] });
        }
      });
    }
  });
});
