// Fill the DB with example data on startup

import { Meteor } from "meteor/meteor";
import Tag from "/imports/api/tags/tags.js";
import Edge from "/imports/api/edges/edges.js";
import Node from "/imports/api/nodes/nodes.js";
import Link from "/imports/api/links/links.js";

Meteor.startup(() => {

});
