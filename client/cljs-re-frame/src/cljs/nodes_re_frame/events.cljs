(ns nodes-re-frame.events
  (:require [re-frame.core :refer [reg-event-db reg-event-fx reg-fx]]
            [day8.re-frame.http-fx]                         ;; even if we don't use this require its existence will cause the :http-xhrio effect handler to self-register with re-frame
            [ajax.core :refer [json-request-format json-response-format]]
            [clojure.string :as str]
            [nodes-re-frame.db :as db]
            [nodes-re-frame.api :as api]))

;; ----------------------------------------------------------------------
;; -------------------------------- CORE --------------------------------
;; ----------------------------------------------------------------------

;; ----- Helpers -----
(def api-url "http://localhost:3000")

(defn endpoint [& params]
  "Concat any params to api-url separated by /"
  (str/join "/" (concat [api-url] params)))

;; ----- Initiate -----
(reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

;; ----- Navigation -----
(reg-event-db
 :navigate-to-page
 (fn [db [_ page init]]
   (-> db
       (merge db/default-db)
       (merge init)
       (assoc :page page))))

(reg-event-db
 :open-search-nav
 (fn [db [_]]
   (assoc db :show-search-nav true)))

(reg-event-db
 :close-search-nav
 (fn [db [_]]
   (assoc db :show-search-nav false)))

(reg-event-db
 :open-dialog
 (fn [db [_ dialog]]
   (assoc db :show-dialog dialog)))

(reg-event-db
 :close-dialog
 (fn [db [_]]
   (assoc db :show-dialog :none)))

;; ----- Search -----
(reg-event-fx
 :search
 (fn [{:keys [db]} [_ query]]
   {:http-xhrio (api/search-concepts query)
    :db         (-> db
                    ;clear search results so the user won't be confused by previous results
                    (assoc :search [])
                    ;set loading indication for the user to know we are loading data from the server
                    (assoc-in [:loading :search] true)
                    ;navigating to the search-results page
                    (assoc :page :search-results))}))

;; ----- View Node -----
(reg-event-fx
 :view-node
 (fn [{:keys [db]} [_ concept-id]]
   {:http-xhrio (api/view-concept concept-id)
    :db         (-> db
                    ;clear current node so the user won't be confused by previous results
                    (assoc :current-node nil)
                    ;set loading indication for the user to know we are loading data from the server
                    (assoc-in [:loading :current-node] true)
                    ;navigating to the node's page
                    (assoc :page :node))}))

;; ----- Create Node -----
(reg-event-fx
 :create-node
 (fn [{:keys [db]} [_ concept-name]]
   {:http-xhrio (api/create-concept concept-name)
    :db          (assoc-in db [:loading :create-node] true)}))

(reg-event-fx
 :create-node-request-success
 (fn [{:keys [db]} [_ [response]]]
   {:set-hash {:hash (apply str ["/node/" (:event_id response)])}
    :db       (assoc-in db [:loading :create-node] false)}))

;; ----- Delete Node -----

(reg-event-fx
 :delete-node
 (fn [{:keys [db]} [_ node-id]]
   {:http-xhrio (api/delete-concept (-> db :current-node first :node_id))
    :db          (assoc-in db [:loading :delete-node] true)}))

(reg-event-fx
 :delete-node-request-success
 (fn [{:keys [db]} [_ [response]]]
   {:set-hash {:hash "/"}
    :db       (assoc-in db [:loading :delete-node] false)}))

;; ----- API Calls -----
(reg-event-db
 :api-request-success
 (fn [db [_ path response]]
   (-> db
       (assoc-in (concat [:loading] path) false)
       (assoc-in (concat path) response))))

(reg-event-db
 :api-request-error
 (fn [db [_ path response]]
   (-> db
       (assoc-in (concat [:loading] path) false)
       (assoc-in (concat [:errors] path) response))))

;; ---------------------------------------------------------------------
;; -------------------------------- WEB --------------------------------
;; ---------------------------------------------------------------------

;; ----- Search -----
(reg-event-fx
 :web-search
 (fn [_ [_ query]]
   {:set-hash {:hash (apply str ["/search?q=" query])}}))

;; ----- Effects -----
(reg-fx
 :set-hash
 (fn [{:keys [hash]}]
   (set! (.-hash js/location) hash)))
