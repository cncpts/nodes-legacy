(ns nodes-re-frame.subs
  (:require [re-frame.core :refer [reg-sub]]))

;; -------------------------------------------------------------------------
;; -------------------------------- GENERAL --------------------------------
;; -------------------------------------------------------------------------

(reg-sub
 :page
 (fn [db]
   (get-in db [:page])))

(reg-sub
 :show-search-nav
 (fn [db]
   (get-in db [:show-search-nav])))

(reg-sub
 :dialog
 (fn [db]
   (get-in db [:show-dialog])))

;; -------------------------------------------------------------------------
;; -------------------------------- SEARCH ---------------------------------
;; -------------------------------------------------------------------------

(reg-sub
 :search-results
 (fn [db]
   (get-in db [:search])))

;; -------------------------------------------------------------------------
;; -------------------------------- NODE -----------------------------------
;; -------------------------------------------------------------------------

(reg-sub
 :current-node
 (fn [db]
   (get-in db [:current-node])))
