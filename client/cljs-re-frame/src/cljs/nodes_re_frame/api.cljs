(ns nodes-re-frame.api
  (:require [ajax.core :refer [json-request-format json-response-format]]
            [clojure.string :as str]))

(def ip-api "http://192.168.1.5:3000")
(def local-api "http://localhost:3000")

(def api-url local-api)

(defn endpoint [& params]
  "Concat any params to api-url separated by /"
  (str/join "/" (concat [api-url] params)))

(defn search-concepts [query]
  {:method          :get
   :uri             (endpoint "concept")
   :params          {:concept_name (apply str ["ilike.*" query "*"])}
   :response-format (json-response-format {:keywords? true})
   :on-success      [:api-request-success [:search]]
   :on-failure      [:api-request-error [:search]]})

(defn view-concept [id]
  {:method          :get
   :uri             (endpoint "concept")
   :params          {:concept_id (apply str ["eq." id])}
   :response-format (json-response-format {:keywords? true})
   :on-success      [:api-request-success [:current-node]]
   :on-failure      [:api-request-error [:current-node]]})

(defn create-concept [name]
  {:method          :post
   :uri             (endpoint "concept")
   :params          {:concept_name name}
   :headers         {:Prefer "return=none"}
   :format          (json-request-format)
   :response-format (json-response-format {:keywords? true})
   :on-success      [:create-node-request-success]
   :on-failure      [:api-request-error [:create-node]]})

(defn delete-concept [id]
  {:method          :delete
   :uri             (endpoint "concept")
   :params          {:concept_id (apply str ["eq." id])}
   :headers         {:Prefer "return=representation"}
   :format          (json-request-format)
   :response-format (json-response-format {:keywords? true})
   :on-success      [:delete-node-request-success]
   :on-failure      [:api-request-error [:delete-node]]})
