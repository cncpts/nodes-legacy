(ns nodes-re-frame.views
  (:require [reagent.core :as reagent]
            [re-frame.core :refer [subscribe dispatch]]))

(defn node-ref [url]
  [:li.list-group-item.flex-column.align-items-start.p-1
   {:key url}
   [:div.d-flex.w-100.justify-content-start
    {:style {:overflow-x "auto"}}
    [:a.text-nowrap {:href url} url]]
   [:div.d-flex.w-100.justify-content-start
    {:style {:overflow-x "auto"}}
    [:span.badge.badge-primary.mr-1 "some"]
    [:span.badge.badge-primary.mr-1 "tags"]]])

(defn node-card [{:keys [node_id node_name]}]
  [:a.list-group-item.list-group-item-action.p-1
   {:key node_id
    :href (apply str ["/#/node/" node_id])}
   [:h4 node_name]
   [:p "Short description about the node"]])

(defn delete-node-dialog []
  [:div.modal.modal-open {:style {:display "block" :background-color "rgba(0,0,0,0.5)"} :on-click #(dispatch [:close-dialog])}
   [:div.modal-dialog.modal-dialog-centered
    [:div.modal-content
     [:div.modal-header
      [:h5.modal-title "Are you sure?"]
      [:button.close {:on-click #(dispatch [:close-dialog])}
       [:span "×"]]]
     [:div.modal-body.d-flex.justify-content-around
      [:button.btn.btn-danger {:on-click #(dispatch [:delete-node])} "Yes"]
      [:button.btn.btn-secondary {:on-click #(dispatch [:close-dialog])} "No"]]]]])

;; -----------------------------------------------------------------------
;; -------------------------------- PAGES --------------------------------
;; -----------------------------------------------------------------------

(defn create-node-page []
  (let [node-name (reagent/atom "")
        create-node #(dispatch [:create-node @node-name])]
    (fn [] [:div.container
            [:br]
            [:div.row
             [:div.col [:h1.text-center "Create Node"]]]
            [:div.row
             [:div.col
              [:div.form-group
               [:label.col-form-label.col-form-label-lg {:for "node-name"} "Node Name:"]
               [:input.form-control.form-control-lg
                {:placeholder "Enter a name of the new node"
                 :value        @node-name
                 :on-change    #(reset! node-name (-> % .-target .-value))
                 :on-key-down  #(case (.-which %)
                                  13 (create-node)
                                  nil)
                 :type         "text"}]]]]
            [:hr]
            [:div.row
             [:div.col.text-center
              [:button.btn.btn-primary.btn-lg
               {:on-click create-node}
               "Create Node"]]]])))

(defn node-page []
  (let [[node] @(subscribe [:current-node])]
    [:div.container
     [:div.row
      [:div.col [:h1 (:node_name node)]]]
     [:div.row
      [:div.col
       [:h4 "node's description based on it's relations"]]]
     [:div.row
      [:div.col
       [:ul.list-group
        (map node-ref (:urls node))]]]]))

(defn search-results-page []
  (let [search-results (subscribe [:search-results])]
    [:ul.list-group
     (map node-card @search-results)]))

(defn home-page []
  [:div
   [:h1 "Home Page"]])

(defn sign-page [page]
  [:div.container
   [:br]
   [:div.row
    [:div.col
     [:div.btn-group.w-100.btn-group-toggle
      [:a.btn.btn-primary.btn-lg.clickable.w-100.text-white
       (merge {:href "/#/login"} (if (= @page :login) {:class "active"} nil))
       "Login"
       [:input {:id "login" :name "options" :type "radio"}]]
      [:a.btn.btn-primary.btn-lg.clickable.w-100.text-white
       (merge {:href "/#/register"} (if (= @page :register) {:class "active"} nil))
       "Register"
       [:input {:id "register" :name "options" :type "radio"}]]]]]
   [:br]
   [:div.row
    [:div.col
     [:div.form-group
      [:input.form-control.form-control-lg {:placeholder "Enter email" :type "email"}]]
     [:div.form-group
      [:input.form-control.form-control-lg {:placeholder "Password" :type "password"}]]
     (if (= @page :register)
       [:div.form-group
        [:input.form-control.form-control-lg {:placeholder "Confirm password" :type "password"}]]
       nil)]]
   [:hr]
   [:div.row
    [:div.col
     [:button.btn.btn-outline-primary.btn-block.btn-lg
      (if (= @page :register)
        "Register"
        "Login")]]]])

;; -------------------------------------------------------------------------
;; -------------------------------- GENERAL --------------------------------
;; -------------------------------------------------------------------------

(defn action-button [action icon]
  [:button.btn.btn-lg.btn-link
   {:on-click action}
   icon])

(defn ab-add-node []
  [:a.btn.btn-lg.btn-link
   {:href "/#/create-node" :role "button"}
   [:i.fas.fa-2x.fa-genderless]])

(defn ab-add-relation []
  [action-button #(dispatch [:close-dialog]) [:i.fas.fa-lg.fa-code-branch]])

(defn ab-add-reference []
  [action-button #(dispatch [:close-dialog]) [:i.fas.fa-lg.fa-link]])

(defn ab-delete-node []
  [action-button #(dispatch [:close-dialog]) [:i.fas.fa-lg.fa-trash]])

(defn action-buttons []
  (let [page @(subscribe [:page])]
    [:div.d-flex.justify-content-between
     (if (some #{page} '(:home :search-results :node :create-node)) [ab-add-node] nil)
     (if (some #{page} '(:home :search-results :node :create-node)) [ab-add-relation] nil)
     (if (some #{page} '(:home :search-results :node :create-node)) [ab-add-reference] nil)
     (if (some #{page} '(:node)) [ab-delete-node] nil)]))

(defn action-grid []
  [:div.modal.modal-open {:style {:display "block" :background-color "rgba(0,0,0,0.4)"} :on-click #(dispatch [:close-dialog])}
   [:div.modal-dialog.d-flex.align-items-end.h-100.m-0.animated.slideInUp
    [:div.modal-content
     [:div.modal-body
      [action-buttons]]]]])

(defn floating-action-button [page]
  (if (some #{@page} '(:home :search-results :node :create-node))
     [:button.btn.btn-lg.btn-outline-primary.fixed-bottom.m-4
      {:style {:height "52px" :width "52px" :left "unset"}
       :on-click #(dispatch [:open-dialog :action-grid])}
      [:i.fas.fa-plus]]
    nil))

(defn nav []
  [:nav.navbar.navbar-light.bg-light
   [:a.navbar-brand {:href "/#/"} "Nodes"]
   [:ul.navbar-nav.flex-row
    [:li.nav-item [:a.nav-link.px-3 {:on-click #(dispatch [:open-search-nav])} [:i.fas.fa-lg.fa-search]]]
    [:li.nav-item [:a.nav-link.px-3 {:href "/#/login"} [:i.fas.fa-lg.fa-user]]]]])

(defn search-nav []
  (let [query (reagent/atom "")
        search #(dispatch [:web-search @query])]
    (fn []
      [:nav.navbar.navbar-light.bg-light
       [:ul.navbar-nav.pr-2
        [:li.nav-item [:a.nav-link {:on-click #(dispatch [:close-search-nav])} [:i.fas.fa-lg.fa-arrow-left]]]]
       [:div.form-inline.flex-nowrap.flex-grow-1
        [:input.form-control.mx-2
         {:type "text"
          :placeholder "Search"
          :autoComplete "off"
          :value @query
          :on-change    #(reset! query (-> % .-target .-value))
          :on-key-down  #(case (.-which %)
                           13 (search)
                           nil)}]
        [:button.btn.btn-link.text-secondary.px-1
         {:type "submit" :on-click search}
         [:i.fas.fa-lg.fa-search]]]])))

(defn display-dialog [dialog]
  (case @dialog
    :delete-node [delete-node-dialog]
    :action-grid [action-grid]
    :none nil
    nil))

(defn display-page [page]
  (case @page
    :home [home-page]
    :register [sign-page page]
    :login [sign-page page]
    :search-results [search-results-page]
    :node [node-page]
    :create-node [create-node-page]
    [:div]))

(defn app []
  (let [page (subscribe [:page])
        dialog (subscribe [:dialog])
        show-search-nav @(subscribe [:show-search-nav])]
    [:div
     (if show-search-nav [search-nav] [nav])
     [display-page page]
     [floating-action-button page]
     [display-dialog dialog]]))
