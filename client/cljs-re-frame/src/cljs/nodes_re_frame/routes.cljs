(ns nodes-re-frame.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import goog.History)
  (:require [secretary.core :as secretary]
            [goog.events :as gevents]
            [goog.history.EventType :as EventType]
            [re-frame.core :refer [dispatch]]))

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  ;; --------------------
  (defroute "/" [] (dispatch [:navigate-to-page :home]))
  (defroute "/login" [] (dispatch [:navigate-to-page :login]))
  (defroute "/register" [] (dispatch [:navigate-to-page :register]))
  (defroute "/search" [query-params] (dispatch [:search (:q query-params)]))
  (defroute "/node/:id" [id] (dispatch [:view-node id]))
  (defroute "/create-node" [] (dispatch [:navigate-to-page :create-node]))
  (hook-browser-navigation!))
