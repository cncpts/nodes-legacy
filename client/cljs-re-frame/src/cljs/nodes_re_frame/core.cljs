(ns nodes-re-frame.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [nodes-re-frame.subs]
            [nodes-re-frame.events :as events]
            [nodes-re-frame.routes :as routes]
            [nodes-re-frame.views :as views]
            [nodes-re-frame.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/app]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
