export const ROUTES = {
  HOME: "/",
  SEARCH: "/search",
  LOGIN: "/login",
  REGISTER: "/register",
  NODE_PAGE: "/node",
  CREATE_NODE: "/create-node",
  ADD_RELATION: "/add-relation",
  ADD_REFERENCE: "/add-reference"
};
