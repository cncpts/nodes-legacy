import React from "react";
import Navbar from "./Navbar";
import { ActionGrid, FABRouter } from "./ActionGrid";
import DeleteNodeDialog from "./DeleteNodeDialog";

const Layout = ({ children }) => (
  <div className="h-100">
    <Navbar />
    <FABRouter />
    <ActionGrid />
    <DeleteNodeDialog />
    {children}
  </div>
);

export default Layout;
