import React from "react";
import gql from "graphql-tag";
import { NavLink as RRNavLink } from "react-router-dom";
import { withRouter } from "react-router";
import { Navbar as BsNavbar, NavbarBrand } from "reactstrap";
import { Nav, NavItem, NavLink } from "reactstrap";
import { Form, Input, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faUser } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { contains } from "ramda";
import { withApollo } from "../apollo";
import { ROUTES } from "../consts";

const changeNavbar = (client, navbar) =>
  client.writeData({ data: { navbar: navbar } });

const Navbar = withRouter(
  withApollo()(({ client, location: { pathname } }) => {
    return (
      <BsNavbar light className="bg-light py-2">
        <NavbarBrand tag={RRNavLink} to={ROUTES.HOME}>
          Nodes
        </NavbarBrand>
        <Nav className="flex-row" navbar>
          <NavItem>
            <NavLink
              className="px-3 clickable"
              onClick={() => changeNavbar(client, "SEARCH")}
            >
              <FontAwesomeIcon icon={faSearch} size="lg" />
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className="px-3"
              tag={RRNavLink}
              to={ROUTES.LOGIN}
              isActive={() =>
                contains(pathname, [ROUTES.REGISTER, ROUTES.LOGIN])
              }
            >
              <FontAwesomeIcon icon={faUser} size="lg" />
            </NavLink>
          </NavItem>
        </Nav>
      </BsNavbar>
    );
  })
);

const SearchNavbar = withRouter(
  withApollo()(({ client, history }) => (
    <BsNavbar light className="bg-light flex-nowrap py-2">
      <Nav navbar className="pr-2">
        <NavItem>
          <NavLink
            className="clickable"
            onClick={() => changeNavbar(client, "MAIN")}
          >
            <FontAwesomeIcon icon={faArrowLeft} size="lg" />
          </NavLink>
        </NavItem>
      </Nav>
      <Form
        inline
        className="flex-nowrap"
        onSubmit={e => {
          e.preventDefault();
          e.target.query.blur();
          const query = e.target.query.value;
          history.push(`${ROUTES.SEARCH}/${query}`);
        }}
      >
        <Input
          type="search"
          name="query"
          placeholder="Search"
          autoComplete="off"
          className="mx-3"
        />
        <Button color="link" className="clickable text-secondary px-1">
          <FontAwesomeIcon icon={faSearch} size="lg" />
        </Button>
      </Form>
    </BsNavbar>
  ))
);

const GET_NAVBAR = gql`
  {
    navbar @client
  }
`;

const NavbarRouter = withApollo(GET_NAVBAR)(
  ({ data: { navbar } }) => (navbar === "MAIN" ? <Navbar /> : <SearchNavbar />)
);

export default NavbarRouter;
