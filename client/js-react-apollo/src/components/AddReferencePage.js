import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { withApollo } from "../apollo";

const GET_CONCEPTS = gql`
  query Concepts {
    concepts(orderBy: name_ASC) {
      id
      name
    }
  }
`;

const CREATE_REFERENCE = gql`
  mutation CreateReference($conceptId: ID!, $url: String!) {
    createReference(
      data: { concept: { connect: { id: $conceptId } }, url: $url }
    ) {
      id
    }
  }
`;

const NodeOptions = withApollo(GET_CONCEPTS)(({ data: { concepts } }) =>
  concepts.map(c => <option value={c.id}>{c.name}</option>)
);

const CreateReferenceForm = withApollo(CREATE_REFERENCE, {
  name: "createReference"
})(({ createReference }) => (
  <Form
    onSubmit={e => {
      e.preventDefault();
      const conceptId = e.target.conceptId.value;
      const url = e.target.url.value;
      createReference({ variables: { conceptId, url } });
    }}
  >
    <Row>
      <Col>
        <FormGroup>
          <Label for="concept">Node:</Label>
          <Input bsSize="lg" type="select" name="conceptId">
            <NodeOptions />
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="url">Reference:</Label>
          <Input bsSize="lg" name="url" placeholder="Type an URL" />
        </FormGroup>
      </Col>
    </Row>
    <hr />
    <Row>
      <Col className="text-center">
        <Button outline color="primary" size="lg">
          Add Reference
        </Button>
      </Col>
    </Row>
  </Form>
));

export default () => {
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h3 className="text-center">Add Reference</h3>
        </Col>
      </Row>
      <br />
      <CreateReferenceForm />
    </Container>
  );
};
