import React from "react";
import { Container, Row, Col } from "reactstrap";
import gql from "graphql-tag";
import { Link } from "react-router-dom";
import { withApollo } from "../apollo";
import { Node, mapNodeToView } from "./Node";
import { ROUTES } from "../consts";

const SEARCH_CONCEPTS = gql`
  query Concepts($query: String!) {
    concepts(where: { name_contains: $query }) {
      id
      name
    }
  }
`;

const Search = ({ data: { concepts } }) => (
  <Container className="py-3">
    {concepts.map((node, i) => (
      <Row key={node.id}>
        <Col
          className={`d-flex justify-content-${i % 2 === 0 ? "start" : "end"}`}
        >
          <Link to={`${ROUTES.NODE_PAGE}/${node.id}`}>
            <Node node={mapNodeToView(node)} />
          </Link>
        </Col>
      </Row>
    ))}
  </Container>
);

export default withApollo(SEARCH_CONCEPTS, {
  options: ({
    match: {
      params: { query = "" }
    }
  }) => ({ variables: { query } })
})(Search);
