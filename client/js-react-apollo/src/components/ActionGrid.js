import React from "react";
import { compose } from "react-apollo";
import gql from "graphql-tag";
import { withRouter } from "react-router";
import { Switch, Route } from "react-router-dom";
import { Button } from "reactstrap";
import { Modal, ModalBody } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGenderless, faCodeBranch } from "@fortawesome/free-solid-svg-icons";
import { faLink, faTrash, faPlus } from "@fortawesome/free-solid-svg-icons";
import { withApollo } from "../apollo";
import { ROUTES } from "../consts";

const ActionButton = ({ action, icon, size }) => {
  return (
    <Button size="lg" color="link" onClick={action}>
      <FontAwesomeIcon icon={icon} size={size} />
    </Button>
  );
};

const navAction = ({ client, history }, href) => () => {
  history.push(href);
  client.resetStore();
};

const IS_ACTION_GRID_OPEN = gql`
  {
    isActionGridOpen @client
  }
`;
const ActionGridModal = ({ history, client, data: { isActionGridOpen } }) => (
  <Modal
    isOpen={isActionGridOpen}
    fade={false}
    toggle={() => client.writeData({ data: { isActionGridOpen: false } })}
    className="d-flex align-items-end h-100 m-0 animated slideInUp"
    contentClassName="rounded-0 w-100"
  >
    <ModalBody className="d-flex justify-content-between">
      <ActionButton
        icon={faGenderless}
        action={navAction({ client, history }, ROUTES.CREATE_NODE)}
        size="2x"
      />
      <ActionButton
        icon={faCodeBranch}
        size="lg"
        action={navAction({ client, history }, ROUTES.ADD_RELATION)}
      />
      <ActionButton
        icon={faLink}
        size="lg"
        action={navAction({ client, history }, ROUTES.ADD_REFERENCE)}
      />
      <Route
        path={`${ROUTES.NODE_PAGE}/:id`}
        component={() => (
          <ActionButton
            icon={faTrash}
            size="lg"
            action={() =>
              client.writeData({ data: { isDeleteNodeDialogOpen: true } })
            }
          />
        )}
      />
    </ModalBody>
  </Modal>
);
export const ActionGrid = compose(
  withRouter,
  withApollo(IS_ACTION_GRID_OPEN)
)(ActionGridModal);

const FAB = withApollo()(({ client }) => {
  const style = {
    height: "64px",
    width: "64px",
    left: "unset"
  };
  return (
    <Button
      color="primary"
      size="lg"
      className="fixed-bottom m-4 rounded-circle"
      style={style}
      onClick={() => client.writeData({ data: { isActionGridOpen: true } })}
    >
      <FontAwesomeIcon icon={faPlus} size="lg" />
    </Button>
  );
});

export const FABRouter = () => (
  <Switch>
    <Route path={ROUTES.SEARCH} component={FAB} />
    <Route path={ROUTES.CREATE_NODE} component={FAB} />
    <Route path={ROUTES.ADD_RELATION} component={FAB} />
    <Route path={ROUTES.ADD_REFERENCE} component={FAB} />
    <Route path={`${ROUTES.NODE_PAGE}/:id`} component={FAB} />
    <Route exact path={ROUTES.HOME} component={FAB} />
  </Switch>
);
