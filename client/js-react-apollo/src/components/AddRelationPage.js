import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { withApollo } from "../apollo";

const GET_CONCEPTS = gql`
  query Concepts {
    concepts(orderBy: name_ASC) {
      id
      name
    }
  }
`;

const NodeOptions = withApollo(GET_CONCEPTS)(({ data: { concepts } }) =>
  concepts.map(c => <option value={c.id}>{c.name}</option>)
);

const CREATE_RELATION = gql`
  mutation CreateRelation($from: ID!, $to: ID!, $description: String!) {
    createRelation(
      data: {
        from: { connect: { id: $from } }
        to: { connect: { id: $to } }
        description: $description
      }
    ) {
      id
      description
    }
  }
`;

export default () => {
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h1 className="text-center">Add Relation</h1>
        </Col>
      </Row>
      <br />
      <Mutation mutation={CREATE_RELATION}>
        {createRelation => (
          <Form
            onSubmit={e => {
              e.preventDefault();
              const from = e.target.from.value;
              const to = e.target.to.value;
              const description = e.target.description.value;
              if (from !== to)
                createRelation({ variables: { from, to, description } });
            }}
          >
            <Row>
              <Col>
                <FormGroup>
                  <Label for="from">From:</Label>
                  <Input type="select" name="from">
                    <NodeOptions />
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="to">To:</Label>
                  <Input type="select" name="to">
                    <NodeOptions />
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="description">Description:</Label>
                  <Input
                    size="lg"
                    name="description"
                    placeholder="Describe the relation"
                  />
                </FormGroup>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col className="text-center">
                <Button outline color="primary" size="lg">
                  Add Relation
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Mutation>
    </Container>
  );
};
