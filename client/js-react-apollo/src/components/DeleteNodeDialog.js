import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { withRouter } from "react-router";
import { Route } from "react-router-dom";
import { Button } from "reactstrap";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { withApollo } from "../apollo";
import { ROUTES } from "../consts";

const closeDialog = client => () =>
  client.writeData({ data: { isDeleteNodeDialogOpen: false } });

const IS_DELETE_DIALOG_OPEN = gql`
  {
    isDeleteNodeDialogOpen @client
  }
`;

const DELETE_CONCEPT = gql`
  mutation DeleteConcept($id: ID!) {
    deleteConcept(where: { id: $id }) {
      id
      name
    }
  }
`;

const DeleteNodeDialog = withRouter(
  withApollo(IS_DELETE_DIALOG_OPEN)(({ match, client, data }) => {
    const id = match.params.id;
    return (
      <Mutation mutation={DELETE_CONCEPT}>
        {deleteConcept => (
          <Modal
            isOpen={data.isDeleteNodeDialogOpen}
            fade={false}
            toggle={closeDialog(client)}
            className="modal-dialog-centered"
          >
            <ModalHeader toggle={closeDialog(client)}>
              Are you sure?
            </ModalHeader>
            <ModalBody className="d-flex justify-content-around">
              <Button
                color="danger"
                onClick={() => deleteConcept({ variables: { id } })}
              >
                <FontAwesomeIcon icon={faCheck} size="lg" />
              </Button>
              <Button color="secondary" onClick={closeDialog(client)}>
                <FontAwesomeIcon icon={faTimes} size="lg" />
              </Button>
            </ModalBody>
          </Modal>
        )}
      </Mutation>
    );
  })
);

export default () => (
  <Route path={`${ROUTES.NODE_PAGE}/:id`} component={DeleteNodeDialog} />
);
