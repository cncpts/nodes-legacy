import React from "react";
import { Button, ButtonGroup } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input } from "reactstrap";
import { withRouter } from "react-router";
import { Route } from "react-router-dom";
import { ROUTES } from "../consts";

const LoginNavs = withRouter(({ location, history }) => {
  return (
    <Row>
      <Col>
        <FormGroup>
          <ButtonGroup className="w-100">
            <Button
              size="lg"
              outline={location.pathname !== ROUTES.LOGIN}
              className="w-100 clickable"
              color="primary"
              onClick={() => history.replace(ROUTES.LOGIN)}
            >
              Login
            </Button>
            <Button
              size="lg"
              outline={location.pathname !== ROUTES.REGISTER}
              className="w-100 clickable"
              color="primary"
              onClick={() => history.replace(ROUTES.REGISTER)}
            >
              Register
            </Button>
          </ButtonGroup>
        </FormGroup>
      </Col>
    </Row>
  );
});

const onSign = e => {
  e.preventDefault();
  const email = e.target.email.value;
  const password = e.target.password.value;
  console.log({ variables: { email, password } });
};

const LoginForms = () => {
  return (
    <Form onSubmit={onSign}>
      <Row>
        <Col>
          <FormGroup>
            <Input size="lg" type="email" name="email" placeholder="Email" />
          </FormGroup>
          <FormGroup>
            <Input
              size="lg"
              type="password"
              name="password"
              placeholder="Password"
            />
          </FormGroup>
          <Route
            path={ROUTES.REGISTER}
            component={() => (
              <FormGroup>
                <Input
                  size="lg"
                  type="password"
                  name="confirmPassword"
                  placeholder="Confirm Password"
                />
              </FormGroup>
            )}
          />
        </Col>
      </Row>
      <hr />
      <Row>
        <Col>
          <Button size="lg" outline block color="primary">
            <Route path={ROUTES.LOGIN} component={() => "Login"} />
            <Route path={ROUTES.REGISTER} component={() => "Register"} />
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

export default () => {
  return (
    <Container className="h-75">
      <br />
      <Row className="align-items-center justify-content-center h-100">
        <Col md="4" style={{ height: "270px" }}>
          <LoginNavs />
          <LoginForms />
        </Col>
      </Row>
    </Container>
  );
};
