import React from "react";
import { Container, Row, Col, Table } from "reactstrap";
import Color from "color";
import gql from "graphql-tag";
import { withApollo } from "../apollo";

const colors = [
  "rgb(26, 188, 15)",
  "rgb(46, 204, 113)",
  "rgb(52, 152, 219)",
  "rgb(155, 89, 182)",
  "rgb(52, 73, 94)",
  "rgb(22, 160, 133)",
  "rgb(39, 174, 96)",
  "rgb(41, 128, 185)",
  "rgb(142, 68, 173)",
  "rgb(44, 62, 80)",
  "rgb(241, 196, 15)",
  "rgb(230, 126, 34)",
  "rgb(231, 76, 60)",
  "rgb(236, 240, 241)",
  "rgb(149, 165, 166)",
  "rgb(243, 156, 18)",
  "rgb(211, 84, 0)",
  "rgb(192, 57, 43)",
  "rgb(189, 195, 199)",
  "rgb(127, 140, 141)"
];

const randomRadius = () => Math.floor(Math.random() * 100 + 100);

export const mapNodeToView = ({ id, name }) => ({
  id,
  name,
  radius: randomRadius(),
  color: colors[Math.floor(Math.random() * colors.length)]
});

export const Node = ({ node, viewNode }) => (
  <div
    className="animated zoomIn node"
    onClick={viewNode}
    style={{
      width: `${node.radius}px`,
      height: `${node.radius}px`,
      background: node.color,
      fontSize: `${(node.radius - node.name.length) / 7}px`,
      color: `${Color(node.color).isLight() ? "black" : "white"}`
    }}
  >
    {node.name}
  </div>
);

const ReferencesTable = ({ refs }) => (
  <Table bordered size="sm">
    <tbody>
      {refs.map(r => (
        <tr className="table-light" key={r.id}>
          <td>
            <a href={r.url}>{r.url}</a>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

const Description = ({ rels }) => {
  return rels.map(r => {
    return <p key={r.id}>{r.description}.</p>;
  });
};

const GET_CONCEPT = gql`
  query Concept($id: ID!) {
    concept(where: { id: $id }) {
      id
      name
      parents {
        id
        description
        to {
          id
          name
        }
      }
      references {
        id
        url
        tags {
          id
          name
        }
      }
    }
  }
`;

const NodePage = ({ data: { concept } }) => {
  return (
    <Container>
      <Row className="py-5">
        <Col className="d-flex justify-content-center">
          <Node node={mapNodeToView(concept)} />
        </Col>
      </Row>
      <Row>
        <Col>
          <Description rels={concept.parents} />
        </Col>
      </Row>
      <Row>
        <Col>
          <ReferencesTable refs={concept.references} />
        </Col>
      </Row>
    </Container>
  );
};

export default withApollo(GET_CONCEPT, {
  options: ({
    match: {
      params: { id }
    }
  }) => ({
    variables: { id }
  })
})(NodePage);
