import React from "react";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Form, FormGroup, Input, Label } from "reactstrap";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const CREATE_CONCEPT = gql`
  mutation CreateConcept($name: String!) {
    createConcept(data: { name: $name }) {
      id
      name
    }
  }
`;

export default () => {
  return (
    <Container>
      <br />
      <Row>
        <Col>
          <h1 className="text-center">New Concept</h1>
        </Col>
      </Row>
      <br />
      <Mutation mutation={CREATE_CONCEPT}>
        {createConcept => (
          <Form
            onSubmit={e => {
              e.preventDefault();
              const name = e.target.name.value;
              createConcept({ variables: { name } });
            }}
          >
            <Row>
              <Col>
                <FormGroup>
                  <Label for="name">Concept Name:</Label>
                  <Input bsSize="lg" name="name" placeholder="Type a name" />
                </FormGroup>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col className="text-center">
                <Button outline color="primary" size="lg">
                  Create Concept
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Mutation>
    </Container>
  );
};
