import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import "./flatly.css";
import { createClient } from "./apollo";
import Router from "./Router";

const App = () => (
  <ApolloProvider client={createClient()}>
    <Router />
  </ApolloProvider>
);

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
