import ApolloClient from "apollo-boost";
import React from "react";
import { compose, graphql, withApollo as apolloConsumer } from "react-apollo";
import { branch, renderComponent } from "recompose";
// import jwtDecode from "jwt-decode";

const Loading = () => <p>Loading...</p>;
const ErrorComponent = ({ data }) => <p>{data.error.toString()}</p>;

export const withApollo = (gql, config) =>
  gql
    ? compose(
        apolloConsumer,
        graphql(gql, config),
        branch(p => p.data && p.data.loading, renderComponent(Loading)),
        branch(p => p.data && p.data.error, renderComponent(ErrorComponent))
      )
    : apolloConsumer;

// Client State
const clientState = {
  defaults: {
    navbar: "MAIN",
    isActionGridOpen: false,
    isDeleteNodeDialogOpen: false
  },

  resolvers: {},

  typeDefs: `
    enum Navbar {
      MAIN
      SEARCH
    }

    type Query {
      navbar: Navbar
      isActionGridOpen: Boolean!
      isDeleteNodeDialogOpen: Boolean!
    }
  `
};

const PRISMA_URI =
  window.location.hostname === "localhost" ? "http://localhost:4466" : "/api";

export const createClient = () => {
  const client = new ApolloClient({
    uri: PRISMA_URI,
    clientState
  });

  return client;
};
