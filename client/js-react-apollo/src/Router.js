import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Layout from "./components/Layout";
import Home from "./components/Home";
import Search from "./components/Search";
import Login from "./components/Login";
import CreateNodePage from "./components/CreateNodePage";
import AddRelationPage from "./components/AddRelationPage";
import AddReferencePage from "./components/AddReferencePage";
import NodePage from "./components/Node";
import { ROUTES } from "./consts";

export default () => (
  <BrowserRouter>
    <Layout>
      <Switch>
        <Route path={`${ROUTES.SEARCH}/:query?`} component={Search} />
        <Route path={ROUTES.LOGIN} component={Login} />
        <Route path={ROUTES.REGISTER} component={Login} />
        <Route path={`${ROUTES.NODE_PAGE}/:id`} component={NodePage} />
        <Route path={ROUTES.CREATE_NODE} component={CreateNodePage} />
        <Route path={ROUTES.ADD_RELATION} component={AddRelationPage} />
        <Route path={ROUTES.ADD_REFERENCE} component={AddReferencePage} />
        <Route component={Home} />
      </Switch>
    </Layout>
  </BrowserRouter>
);
